# reduce

Aimed to be a command line tool to smartly convert videos using the libx264 codec to the libx265 codec.  
Note: you need to have `ffmpeg` and `rust` already installed and in the PATH. 

## Todo
- Adding more video verification options.
- Adding deletion feature upon conversion.
- Some sample tests (unit tests and integration tests)
- Adding environment variables for smart inferencing.
- Adding parallel conversions.
- Adding conversion status (conversion log).  

## Usage
To use this code, do the following:  
Step 1. Clone this repo and navigate into it:  
```
cd reduce
```  
Step 2: Build the project by running:  
```
cargo build
```

To do the conversion, run the command with this syntax:  
```
cargo run -- -i /path/to/video1.extension /path/to/more_videos_seperated_by_space -c:v codec output_format
```
<u>currently supporting:</u>  
codec: 264, 265, x264, x265, h264, h265, libx264, libx265  
output_format: mp4, mkv, avi, flv  



