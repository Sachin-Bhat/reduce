use std::{env, process};

use reduce::Config;

fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem occured while parsing arguments: {}", err);
        process::exit(1);
    });

    if let Err(e) = reduce::convert(config) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}
