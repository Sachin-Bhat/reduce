use std::{error::Error, fs::File, iter::FromIterator, process::Command};

pub fn convert(config: Config) -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::new("ffmpeg");

    for item in &config.file_list {
        let mut _file = match File::open(&item) {
            Err(why) => {
                eprintln!("Couldn't open {}: {}", item, why);
                continue;
            }
            Ok(file) => file,
        };

        cmd.args([
            &config.flag_i,
            &item,
            &config.flag_cv,
            &config.codec,
            &(item.split(".").collect::<Vec<&str>>()[0].to_owned() + "." + &config.output),
        ]);

        cmd.output().expect("Internal error");
    }

    Ok(())
}

pub struct Config {
    pub flag_i: String,
    pub file_list: Vec<String>,
    pub flag_cv: String,
    pub codec: String,
    pub output: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 6 {
            return Err("Not enough arguments");
        }

        let flag_i = args[1].clone();

        let i = match args.iter().position(|x| x == "-c:v") {
            Some(i) => i,
            None => return Err("No codec flag specified: add -c:v"),
        };

        let file_list = Vec::from_iter(args[2..i].iter().cloned());

        let flag_cv = args[i].clone();

        let mut codec = args[i + 1].clone().to_lowercase();

        if codec == "h264" || codec == "x264" || codec == "264" || codec == "libx264" {
            codec = String::from("libx264");
        } else if codec == "h265" || codec == "x265" || codec == "265" || codec == "libx265" {
            codec = String::from("libx265");
        } else {
            eprintln!("Unsupported codec: {}", codec);
        }

        let mut output = args[i + 2].clone().to_lowercase();

        if output == String::from("mp4") || output == String::from(".mp4") {
            output = String::from("mp4");
        } else if output == String::from("mkv") || output == String::from(".mkv") {
            output = String::from("mkv");
        } else if output == String::from("avi") || output == String::from(".avi") {
            output = String::from("avi");
        } else if output == String::from("flv") || output == String::from(".flv") {
            output = String::from("flv");
        } else {
            eprintln!("Unsupported format: {}", output);
        }

        Ok(Config {
            flag_i,
            file_list,
            flag_cv,
            codec,
            output,
        })
    }
}
